$verMin = [Version]'7.1.9'

# version 7.2 and above
if (($host.name -eq 'ConsoleHost') -and ($PSVersionTable.PSVersion -ge $verMin))
{
    Import-Module Az.Tools.Predictor
	Set-PSReadLineOption -PredictionSource HistoryAndPlugin
}
else
{
	Set-PSReadLineOption -PredictionSource History
}

#
Set-PSReadLineOption -PredictionViewStyle ListView

#
#Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
#Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward